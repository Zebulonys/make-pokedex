import sys
import os

import requests
from unidecode import unidecode
import json

from PIL import Image
from io import BytesIO

def get_pokemon_data(url: str, max_generation: int) -> list:
    """
    Fetches Pokemon data from the API for generations from 1 to max_generation.

    Args:
        url (str): The URL of the API.
        max_generation (int): The maximum generation to retrieve data for.

    Returns:
        list: A list containing Pokemon data for all generations.
    """
    pokemon_data = []
    headers = {
        "User-Agent": "RobotPokemon",
        "From": "adresse[at]domaine[dot]com",
        'Content-type': 'application/json'
    }

    for gen in range(1, max_generation + 1):
        if gen > 0:
            request_url = f"{url}/gen/{gen}"
        response = requests.get(request_url, headers=headers)
        if response.status_code == 200:
            data = response.json()
            pokemon_data.extend(data)
        else:
            print(f"Request for generation {max_generation} failed with status code:", response.status_code)
    
    return pokemon_data


def format_pokemon_data(data: list) -> json:
    """
    Formats Pokemon data into a consistent structure.

    Args:
        data (list): A list containing Pokemon data.

    Returns:
        list: A list of formatted Pokemon data.
    """
    formatted_data = []
    for pokemon in data:
        new_pokemon = {
            '_id': pokemon['pokedex_id'],
            'name': unidecode(pokemon['name']['fr']),
            'category': unidecode(pokemon['category']),
            'types': [unidecode(poketype['name']) for poketype in pokemon['types']],
            'sprite': download_pokemon_sprite(pokemon['sprites']['regular'], unidecode(pokemon['name']['fr'].lower()))
        }
        formatted_data.append(new_pokemon)
    return formatted_data
        
def download_pokemon_sprite(url: str, pokename:str, save_folder_path:str = 'data/images') -> str:
    """
    Downloads a Pokemon sprite image from a given URL and saves it to a specified folder.

    Args:
        url (str): The URL of the Pokemon sprite image to download.
        pokename (str): The name of the Pokemon. Used as part of the filename.
        save_folder_path (str, optional): The path to the folder where the image will be saved.
            Defaults to 'data/images'.

    Returns:
        str: The file path of the downloaded image if successful, otherwise None.
    """
    path = f"{save_folder_path}/{pokename}.png"
    response = requests.get(url)
    
    if response.status_code == 200:
        # Ensure the specified save folder exists, create it if necessary
        if not os.path.exists(save_folder_path):
            os.makedirs(save_folder_path)

        image = Image.open(BytesIO(response.content))
        image.save(path)

        return path
    else:
        print(f"Error while downloading {pokename} sprite!")
    return None


def save_pokedex(pokemons_data: list, filename: str = 'data/pokedex.json') -> None:
    """
    Saves the Pokedex data to a JSON file.

    Args:
        pokedex_data (list): A list containing the Pokedex data.
        filename (str): The filename to save the data to.
    """
    try:
        with open(filename, 'w', encoding='utf-8') as file:
            json.dump(pokemons_data, file, indent=2, ensure_ascii=False)
        print("Pokedex saved successfully")
    except Exception as e:
        print("An error has occured while saving the Pokedex data: ", str(e))


def main():
    if len(sys.argv) != 3:
        print("Usage: python make_pokedex.py <generation> <output_filename>")
        sys.exit(1)

    api_url = "https://tyradex.tech/api/v1"
    generation = int(sys.argv[1])
    path = sys.argv[2]

    try:
        data = get_pokemon_data(url=api_url, max_generation=generation)
        pokedex = format_pokemon_data(data)
        # print(pokedex)
        save_pokedex(pokedex, filename=path)
    except Exception as e:
        print("An error occured", str(e))


if __name__ == "__main__":
    main()
