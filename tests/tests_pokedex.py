import unittest
import json
import os

class TestPokedex(unittest.TestCase):
    def setUp(self) -> None:
        """
        Sets up the test by loading the Pokedex data from the JSON file.
        """
        try:
            with open('data/pokedex.json', 'r', encoding='utf-8') as file:
                self.pokedex = json.load(file)
        except Exception as e:
            print("An error has occured while reading the Pokedex file: ", str(e))

    def test_pokemon_is_unique(self) -> None:
        """
        Tests if each Pokemon in the Pokedex has a unique ID.
        """
        print("Starting test for unique Pokemon...")
        ids = [pokemon['_id'] for pokemon in self.pokedex]
        self.assertEqual(len(ids), len(set(ids)), "Duplicate Pokemon found")
        print("Test for unique Pokemon passed.")

    def test_pokemon_has_max_two_types(self) -> None:
        """
        Tests if each Pokemon in the Pokedex has at most two types.
        """
        print("Starting test for Pokemon sprites...")
        for pokemon in self.pokedex:
            self.assertLessEqual(len(pokemon['types']), 2, f"More than 2 types were found for {pokemon['name']}")
        print("Test for Pokemon sprites passed.")

    def test_pokemon_has_sprite(self) -> None:
        """
        Tests if each Pokémon in the Pokedex has a sprite.
        """
        print("Starting test for maximum two types per Pokemon...")
        for pokemon in self.pokedex:
            sprite_name = f"{pokemon['sprite']}"
            sprite_path = os.path.join(sprite_name)
            self.assertTrue(os.path.exists(sprite_path), f"Sprite was not found for Pokemon {pokemon['name']}")
        print("Test for maximum two types per Pokemon passed.")


if __name__ == '__main__':
    unittest.main()