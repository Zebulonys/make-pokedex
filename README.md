# Make Pokedex

Make Pokedex is a Python script that fetches data from an API to create a Pokédex in JSON format.
This project aims to fetch and parse data of Pokemon from a specific generation to facilitate the creation of a Pokédex and, more generally, a Pokemon game that I am working on. It will evolve according to the needs.

## Installation

1. Clone the repository:

```bash
git clone
```

2. Install dependencies:
```bash
python -m pip install requirements.txt
```
or just:
```bash
pip install requirements.txt
```

## Usage

Run the `make_pokedex.py` script with two arguments: the Pokemon generation and the output filename for the Pokédex JSON file:
```bash
python make_pokedex.py <generation> <output_filename>
```
Replace `<generation>` with the desired Pokemon generation (e.g., 4), and `<output_filename>` with the desired filename for the Pokédex JSON file.
For example : 
```bash
python make_pokedex.py 4 pokedex.json
```
The generated Pokédex data will be saved to a file named `pokedex.json` in the project directory. It will contained datas about Pearl/Diamond.

## Running Tests

To run the unit tests, navigate to the root directory of the project and execute the following command:
```bash
python -m unittest discover -s tests
```
This will execute all the unit tests defined in the `tests` directory and its subdirectories, including `test_pokedex.py`.

## API Used

The script fetches data from the [Tyradex API](https://tyradex.tech/api/v1) to get information about Pokemon.

## Dependencies

- requests
- json
- Unidecode

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
